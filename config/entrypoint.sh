#!/bin/sh
# docker-entrypoint, supervisor starter
set -e

PHP_EXTENSIONS_DIR=$(php -r 'echo ini_get("extension_dir");')

APP_ENV=${APP_ENV:="prod"} 
APP_DEBUG=${APP_DEBUG:="false"} 
NGINX_PORT=${PORT:=80} 
BLACKFIRE_HOST=${BLACKFIRE_HOST:="blackfire"}
BLACKFIRE_PORT=${BLACKFIRE_PORT:="8707"}

echo "Running Application Entrypoint with ENV: $APP_ENV"

# Force install of dependencies when not in production. This is useful when using Docker volumes to mount the application code.
if [ "$APP_ENV" != 'prod' ]; then
    composer install --prefer-dist --no-progress --no-suggest --no-interaction
fi

# Disable XDEBUG in production
if [ "$APP_ENV" = 'prod' ]; then
 	echo "Disabling XDEBUG in production!!"
	rm -r $PHP_CONF_DIR/xdebug.ini
	rm -f $PHP_EXTENSIONS_DIR/xdebug.so
fi

if [ "$BLACKFIRE_ENABLED" = "true" ]; then
	printf "blackfire.agent_socket=tcp://$BLACKFIRE_HOST:$BLACKFIRE_PORT" >> $PHP_CONF_DIR/blackfire.ini
fi

# Running NGINX
if [ "$1" = "--server" ]; then
    echo "Configuring NGINX to serve at Port: $NGINX_PORT"
    sed -i "s/:port:/$NGINX_PORT/" /etc/nginx/sites-enabled/default

    echo "Exporting Application ENV vars to nginx";

	# export application vars to NGINX. Only vars with stat with APP, SYMFONY OR XDEBUG will be exported.
	envvars=$(printenv | grep -E '^APP_*|XDEBUG_*|SYMFONY_*') && for ev in $envvars;
	do
	  oldIFS=$IFS
	  IFS="="
	  set -- $ev

	  echo -e "adding ENV var $1 => $2 to NGINX";
	  sed -i "/# APP/a fastcgi_param $1 '$2';" /etc/nginx/sites-enabled/default
	  IFS=$oldIFS
	done

    exec /usr/local/bin/start_services
fi

docker-php-entrypoint $@

